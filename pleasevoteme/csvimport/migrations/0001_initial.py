# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CSVImport'
        db.create_table(u'csvimport_csvimport', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('model_name', self.gf('django.db.models.fields.CharField')(default='iisharing.Item', max_length=255)),
            ('field_list', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('upload_file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('file_name', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('encoding', self.gf('django.db.models.fields.CharField')(max_length=32, blank=True)),
            ('upload_method', self.gf('django.db.models.fields.CharField')(default='manual', max_length=50)),
            ('error_log', self.gf('django.db.models.fields.TextField')()),
            ('import_date', self.gf('django.db.models.fields.DateField')(auto_now=True, blank=True)),
            ('import_user', self.gf('django.db.models.fields.CharField')(default='anonymous', max_length=255, blank=True)),
        ))
        db.send_create_signal(u'csvimport', ['CSVImport'])

        # Adding model 'ImportModel'
        db.create_table(u'csvimport_importmodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('csvimport', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['csvimport.CSVImport'])),
            ('numeric_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('natural_key', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'csvimport', ['ImportModel'])


    def backwards(self, orm):
        # Deleting model 'CSVImport'
        db.delete_table(u'csvimport_csvimport')

        # Deleting model 'ImportModel'
        db.delete_table(u'csvimport_importmodel')


    models = {
        u'csvimport.csvimport': {
            'Meta': {'object_name': 'CSVImport'},
            'encoding': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'error_log': ('django.db.models.fields.TextField', [], {}),
            'field_list': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'file_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'import_date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'import_user': ('django.db.models.fields.CharField', [], {'default': "'anonymous'", 'max_length': '255', 'blank': 'True'}),
            'model_name': ('django.db.models.fields.CharField', [], {'default': "'iisharing.Item'", 'max_length': '255'}),
            'upload_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'upload_method': ('django.db.models.fields.CharField', [], {'default': "'manual'", 'max_length': '50'})
        },
        u'csvimport.importmodel': {
            'Meta': {'object_name': 'ImportModel'},
            'csvimport': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['csvimport.CSVImport']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'natural_key': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'numeric_id': ('django.db.models.fields.PositiveIntegerField', [], {})
        }
    }

    complete_apps = ['csvimport']