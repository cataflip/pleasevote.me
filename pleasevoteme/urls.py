from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.contrib.sitemaps import GenericSitemap
from elections.models import OnlineCampaign
from helpers.views import TemplateCachedView, handler404, handler500

# Admin
from django.contrib import admin
admin.autodiscover()

handler404 = 'pleasevoteme.helpers.views.handler404'
handler500 = 'pleasevoteme.helpers.views.handler500'

info_dict = {
    'queryset': OnlineCampaign.objects.all(),
    'date_field': 'created',
}

sitemaps = {
    'campaign': GenericSitemap(info_dict, priority=0.9, changefreq='monthly'),
}

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name="index.html")),

    # Apps urls
    url(r'^/?', include('pleasevoteme.elections.urls')),

    # Facebook channel file
    # https://developers.facebook.com/docs/reference/javascript/#channel
    url(r'^channel\.html$', TemplateCachedView.as_view(template_name="channel.html")),

    # General
    url(r'^privacy/$', TemplateView.as_view(template_name='general/privacy.html'), name='privacy'),
    url(r'^tos/$', TemplateView.as_view(template_name='general/tos.html'), name='tos'),
    url(r'^faq/$', TemplateView.as_view(template_name='general/faq.html'), name='faq'),
    url(r'^about/$', TemplateView.as_view(template_name='general/about.html'), name='about'),

    # Admin
    url(r'^adminz/', include(admin.site.urls)),

    # Vendors
    url(r'^ckeditor/', include('ckeditor.urls')),

    # Error page
    url(r'^404/$', handler404, name='404'),
    url(r'^500/$', handler500, name='500'),

    # the sitemap
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps})
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
