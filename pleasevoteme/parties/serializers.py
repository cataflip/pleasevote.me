from rest_framework import serializers

from pleasevoteme.seats.serializers import SeatSerializer


class CoalitionSerializer(serializers.Serializer):
    abbreviation = serializers.CharField(max_length=10)
    name = serializers.CharField(max_length=100)
    logo = serializers.Field(source='logo_thumb.url')
    logo_thumb = serializers.Field(source='logo_thumb.url')


class PartySerializer(serializers.Serializer):
    abbreviation = serializers.CharField(max_length=10)
    name = serializers.CharField(max_length=100)
    logo = serializers.Field(source='logo.url')
    logo_thumb = serializers.Field(source='logo_thumb.url')
    coalition = CoalitionSerializer(required=False)


class CandidateSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100)
    photo = serializers.Field(source='photo.url')
    party = PartySerializer()
    side = serializers.CharField(max_length=5)
    seat_p = SeatSerializer(required=False)
    seat_n = SeatSerializer(required=False)