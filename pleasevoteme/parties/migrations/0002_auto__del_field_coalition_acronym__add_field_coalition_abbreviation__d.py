# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Coalition.acronym'
        db.delete_column(u'parties_coalition', 'acronym')

        # Adding field 'Coalition.abbreviation'
        db.add_column(u'parties_coalition', 'abbreviation',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=10),
                      keep_default=False)

        # Deleting field 'Party.acronym'
        db.delete_column(u'parties_party', 'acronym')

        # Adding field 'Party.abbreviation'
        db.add_column(u'parties_party', 'abbreviation',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=10),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Coalition.acronym'
        raise RuntimeError("Cannot reverse this migration. 'Coalition.acronym' and its values cannot be restored.")
        # Deleting field 'Coalition.abbreviation'
        db.delete_column(u'parties_coalition', 'abbreviation')


        # User chose to not deal with backwards NULL issues for 'Party.acronym'
        raise RuntimeError("Cannot reverse this migration. 'Party.acronym' and its values cannot be restored.")
        # Deleting field 'Party.abbreviation'
        db.delete_column(u'parties_party', 'abbreviation')


    models = {
        u'parties.candidate': {
            'Meta': {'object_name': 'Candidate'},
            'about': ('django.db.models.fields.TextField', [], {'max_length': '500', 'blank': 'True'}),
            'facebook': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': "('user',)", 'max_length': '50', 'populate_from': 'None'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'candidates'", 'null': 'True', 'to': u"orm['parties.Party']"}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'seat_n': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'state_seats'", 'null': 'True', 'to': u"orm['seats.Seat']"}),
            'seat_p': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'parliment_seats'", 'null': 'True', 'to': u"orm['seats.Seat']"}),
            'side': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'twitter': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'parties.coalition': {
            'Meta': {'object_name': 'Coalition'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'parties.party': {
            'Meta': {'object_name': 'Party'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'coalition': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'parties'", 'null': 'True', 'to': u"orm['parties.Coalition']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'seats.seat': {
            'Meta': {'object_name': 'Seat'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'seat_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'seats'", 'to': u"orm['states.State']"})
        },
        u'states.state': {
            'Meta': {'object_name': 'State'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'flag': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['parties']