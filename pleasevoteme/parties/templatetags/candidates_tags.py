from django import template

register = template.Library()

@register.filter()
def state_flag_url(candidate):
    """
    Get posts count for `user`
    {{ candidate|get_state }}
    """
    if candidate.seat_p:
        state = candidate.seat_p.state
    else:
        state = candidate.seat_n.state

    return state.flag_thumb.url