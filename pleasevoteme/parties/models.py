import os
import time
from django.db import models
from autoslug import AutoSlugField
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFit
from pleasevoteme.seats.models import Seat


def candidate_photo_upload_path(instance, filename):
    # Get the epoch time to be appended to the end of the file
    # Cache busting!
    epoch = int(time.mktime(time.gmtime()))

    # save file name as the username
    ext = os.path.splitext(filename)[1]
    filename = '%s_%d%s' % (instance.name_slug.replace("-", "_"), epoch, ext)

    return "candidate/%s" % filename


class Coalition(models.Model):
    abbreviation = models.CharField(max_length=10)
    name = models.CharField(max_length=100)
    logo = models.ImageField(upload_to='coalition', blank=True, null=True)
    logo_thumb = ImageSpecField([ResizeToFit(width=100)], image_field='logo', options={'quality': 90})
    website = models.URLField(blank=True)

    def __unicode__(self):
        return u'%s' % self.name


class Party(models.Model):
    abbreviation = models.CharField(max_length=10)
    name = models.CharField(max_length=100)
    logo = models.ImageField(upload_to='party')
    logo_thumb = ImageSpecField([ResizeToFit(width=100)], image_field='logo', options={'quality': 90})
    coalition = models.ForeignKey(Coalition, related_name='parties')
    website = models.URLField(blank=True)

    class Meta:
        verbose_name_plural = "Parties"

    def __unicode__(self):
        return u'%s' % self.abbreviation
        #return u'%s (%s)' % (self.name, self.abbreviation)


class Candidate(models.Model):
    BN = "BN"
    PR = "PR"
    IND = "IND"

    SIDE_CHOICES = (
        (BN, "BN"),
        (PR, "PR"),
        (IND, "IND"),
    )

    name = models.CharField(max_length=100)
    name_slug = AutoSlugField(populate_from=lambda instance: instance.name.lower(),
                              db_index=True,
                              always_update=True)
    photo = models.ImageField(upload_to=candidate_photo_upload_path, default='candidate/default.jpg')
    party = models.ForeignKey(Party, blank=True, null=True, related_name='candidates')
    about = models.TextField(max_length=500, blank=True)
    side = models.CharField(max_length=5, choices=SIDE_CHOICES)
    seat_p = models.ForeignKey(Seat, blank=True, null=True, related_name='parliment_seats')
    seat_n = models.ForeignKey(Seat, blank=True, null=True, related_name='state_seats')
    facebook = models.URLField(blank=True)
    twitter = models.URLField(blank=True)
    website = models.URLField(blank=True)

    def __unicode__(self):
        return u'%s (%s)' % (self.name, self.party.abbreviation)

    def state(self):
        """
        Get the state where the candidate is contesting
        """
        if self.seat_p:
            state = self.seat_p.state
        else:
            state = self.seat_n.state

        return state