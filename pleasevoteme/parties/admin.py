from django.contrib import admin
from django.db.models import Q
from .models import Coalition, Party, Candidate
from annoying.functions import get_object_or_None
from pleasevoteme.seats.models import Seat
from pleasevoteme.states.models import State

from django.contrib.admin import SimpleListFilter


class SeatPListFilter(SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'parliment seat'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'seat'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return Seat.objects.filter(seat_type=Seat.PARLIMENT).values_list('code', 'code')

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        value = self.value()

        if value:
            return queryset.filter(seat_p__code__exact=self.value())
        else:
            return queryset.all()


class SeatNListFilter(SimpleListFilter):
    title = 'state seat'
    parameter_name = 'seat'

    def lookups(self, request, model_admin):
        return Seat.objects.filter(seat_type=Seat.STATE).values_list('code', 'code')

    def queryset(self, request, queryset):
        value = self.value()

        if value:
            return queryset.filter(seat_n__code__exact=self.value())
        else:
            return queryset.all()


class PartyListFilter(SimpleListFilter):
    title = 'party'
    parameter_name = 'party'

    def lookups(self, request, model_admin):
        return Party.objects.values_list('abbreviation', 'abbreviation')

    def queryset(self, request, queryset):

        value = self.value()

        if value:
            return queryset.filter(party__abbreviation__exact=self.value())
        else:
            return queryset.all()


class StateListFilter(SimpleListFilter):
    title = 'state'
    parameter_name = 'state'

    def lookups(self, request, model_admin):
        return State.objects.values_list('abbreviation', 'name')

    def queryset(self, request, queryset):

        value = self.value()

        state = get_object_or_None(State, abbreviation=value)

        if state:
            return queryset.filter(Q(seat_p__state=state) | Q(seat_n__state=state))
        else:
            return queryset.all()


class CandidateAdmin(admin.ModelAdmin):
    list_display = ('name', 'photo', 'party', 'side', 'seat_p', 'seat_n')
    list_filter = (PartyListFilter, StateListFilter, SeatPListFilter, SeatNListFilter)
    search_fields = ('name',)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "seat_p":
            kwargs["queryset"] = Seat.objects.filter(seat_type=Seat.PARLIMENT)

        if db_field.name == "seat_n":
            kwargs["queryset"] = Seat.objects.filter(seat_type=Seat.STATE)

        if db_field.name == "party":
            kwargs["queryset"] = Party.objects.order_by('abbreviation')

        return super(CandidateAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(Coalition)
admin.site.register(Party)
admin.site.register(Candidate, CandidateAdmin)