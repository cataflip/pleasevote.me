from rest_framework import serializers

from pleasevoteme.states.serializers import StateSerializer


class SeatSerializer(serializers.Serializer):
    code = serializers.CharField(max_length=10)
    seat_type = serializers.CharField(max_length=1)
    name = serializers.CharField(max_length=50)
    state = StateSerializer()