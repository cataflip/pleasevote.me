from django.db import models

from pleasevoteme.states.models import State


class Seat(models.Model):
    PARLIMENT = "P"
    STATE = "N"

    TYPE_CHOICES = (
        (PARLIMENT, "Parliment"),
        (STATE, "State"),
    )

    code = models.CharField(max_length=10)
    seat_type = models.CharField(max_length=1, choices=TYPE_CHOICES)
    name = models.CharField(max_length=50)
    state = models.ForeignKey(State, related_name='seats')

    class Meta:
        verbose_name_plural = "Seat"
        verbose_name_plural = "Seats"

    def __unicode__(self):
        return u'%s - %s' % (self.code, self.name)
        #return u'%s - %s (%s)' % (self.code, self.name, self.state)