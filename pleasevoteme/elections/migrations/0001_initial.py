# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PopOrNot'
        db.create_table(u'elections_popornot', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('short_url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('candidate_a', self.gf('django.db.models.fields.related.ForeignKey')(related_name='as_a_matches', to=orm['parties.Candidate'])),
            ('candidate_b', self.gf('django.db.models.fields.related.ForeignKey')(related_name='as_b_matches', to=orm['parties.Candidate'])),
            ('winner', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='won_matches', null=True, to=orm['parties.Candidate'])),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal(u'elections', ['PopOrNot'])


    def backwards(self, orm):
        # Deleting model 'PopOrNot'
        db.delete_table(u'elections_popornot')


    models = {
        u'elections.popornot': {
            'Meta': {'object_name': 'PopOrNot'},
            'candidate_a': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'as_a_matches'", 'to': u"orm['parties.Candidate']"}),
            'candidate_b': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'as_b_matches'", 'to': u"orm['parties.Candidate']"}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'short_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'winner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'won_matches'", 'null': 'True', 'to': u"orm['parties.Candidate']"})
        },
        u'parties.candidate': {
            'Meta': {'object_name': 'Candidate'},
            'about': ('django.db.models.fields.TextField', [], {'max_length': '500', 'blank': 'True'}),
            'facebook': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': "('user',)", 'max_length': '50', 'populate_from': 'None'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'candidates'", 'null': 'True', 'to': u"orm['parties.Party']"}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'seat_n': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'state_seats'", 'null': 'True', 'to': u"orm['seats.Seat']"}),
            'seat_p': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'parliment_seats'", 'null': 'True', 'to': u"orm['seats.Seat']"}),
            'side': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'twitter': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'parties.coalition': {
            'Meta': {'object_name': 'Coalition'},
            'acronym': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'parties.party': {
            'Meta': {'object_name': 'Party'},
            'acronym': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'coalition': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'parties'", 'null': 'True', 'to': u"orm['parties.Coalition']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'seats.seat': {
            'Meta': {'object_name': 'Seat'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'seat_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'seats'", 'to': u"orm['states.State']"})
        },
        u'states.state': {
            'Meta': {'object_name': 'State'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['elections']