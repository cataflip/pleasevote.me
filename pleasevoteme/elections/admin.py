from django import forms
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget
from .models import OnlineCampaign


class OnlineCampaignAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = OnlineCampaign


class OnlineCampaignAdmin(admin.ModelAdmin):
    form = OnlineCampaignAdminForm

admin.site.register(OnlineCampaign, OnlineCampaignAdmin)