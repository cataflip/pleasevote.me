from django.views.generic.base import TemplateView
from django.views.generic import ListView
from django.http import HttpResponseBadRequest, HttpResponseNotAllowed, HttpResponse, HttpResponseNotFound
from django.db.models import Q
from django.shortcuts import get_object_or_404
from annoying.functions import get_object_or_None
from pleasevoteme.helpers.views import AjaxableResponseMixin, json_success_response, json_error_response
from pleasevoteme.parties.models import Candidate, Coalition
from pleasevoteme.parties.serializers import CandidateSerializer
from .utils import PopOrNotSecret, get_popularity_for_candidate, update_score_for_candidate
from .models import PopOrNot, PopResult, OnlineCampaign


class CampaignListView(ListView):
    queryset = OnlineCampaign.objects.order_by('-created')
    model = OnlineCampaign
    paginate_by = 6
    context_object_name = 'campaigns'
    template_name = 'campaign.html'


class CampaignCoalitionView(ListView):
    model = OnlineCampaign
    paginate_by = 6
    context_object_name = 'campaigns'
    template_name = 'campaign_coalition.html'
    coalition = None

    def get_queryset(self):
        queryset = super(CampaignCoalitionView, self).get_queryset()

        coalition = get_object_or_None(Coalition, abbreviation__iexact=self.kwargs['coalition'])
        self.coalition = coalition
        queryset = queryset.filter(coalition=coalition).select_related()

        return queryset

    def get_context_data(self, **kwargs):
        context = super(CampaignCoalitionView, self).get_context_data(**kwargs)
        context['coalition'] = self.coalition

        return context


class CampaignSingleView(TemplateView):
    template_name = 'campaign_single.html'

    def get_context_data(self, **kwargs):
        context = super(CampaignSingleView, self).get_context_data(**kwargs)

        campaign = get_object_or_404(OnlineCampaign, slug=context['slug'])
        more_campaigns = OnlineCampaign.objects.exclude(id=campaign.id) \
                                               .filter(coalition__id=campaign.coalition.id) \
                                               .select_related().order_by('?')[:2]

        context['campaign'] = campaign
        context['more_campaigns'] = more_campaigns

        return context


class PopOrNotVoteView(TemplateView):
    template_name = 'vote.html'

    def get_context_data(self, **kwargs):
        context = super(PopOrNotVoteView, self).get_context_data(**kwargs)

        candidate_a = Candidate.objects.select_related().order_by('?')[0]  # Might be slow
        candidate_b = Candidate.objects.exclude(side=candidate_a.side).select_related().order_by('?')[0]

        context['candidate_a'] = candidate_a
        context['candidate_b'] = candidate_b

        secret = PopOrNotSecret()

        context['token'] = secret.generate_token(candidate_a, candidate_b)

        return context


class VoteResultView(TemplateView):
    template_name = 'vote_result.html'

    def get_context_data(self, **kwargs):
        context = super(VoteResultView, self).get_context_data(**kwargs)

        result = get_object_or_404(PopResult, result_slug=context['slug'])

        candidate_a = result.candidate_a
        candidate_b = result.candidate_b

        popularity_a, score_a = get_popularity_for_candidate(candidate_a)
        popularity_b, score_b = get_popularity_for_candidate(candidate_b)

        context['result'] = result
        context['candidate_a'] = candidate_a
        context['candidate_b'] = candidate_b
        context['popularity_a'] = popularity_a
        context['popularity_b'] = popularity_b
        context['ranking_a'] = candidate_a.score.ranking()
        context['ranking_b'] = candidate_b.score.ranking()
        context['total_candidates'] = Candidate.objects.count()

        return context


def skip_or_next(request):
    secret = PopOrNotSecret()

    if not request.is_ajax():
        return HttpResponseBadRequest()

    if request.method == "GET":
        vote_token = request.GET.get('token')

        if not vote_token:
            return HttpResponseBadRequest(json_error_response('Token not found.'))

        # OK, now we have to token, it's time to get the candidates
        candidate_a, candidate_b = secret.get_candidates(vote_token, validate_token=False)

        # Get a new candidate A and B
        new_candidate_a = Candidate.objects.exclude(id__in=[candidate_a.id, candidate_b.id]) \
                                           .select_related().order_by('?')[0]

        new_candidate_b = Candidate.objects.filter(~Q(side=new_candidate_a.side)) \
                                           .exclude(id__in=[candidate_a.id, candidate_b.id, new_candidate_a.id]) \
                                           .select_related().order_by('?')[0]

        new_token = secret.generate_token(new_candidate_a, new_candidate_b)

        response_data = {
            'candidate_a': CandidateSerializer(new_candidate_a).data,
            'candidate_b': CandidateSerializer(new_candidate_b).data,
            'token': new_token,
        }

        return HttpResponse(json_success_response(response_data), mimetype='application/json')

    else:
        return HttpResponseNotAllowed(json_error_response('Only GET allowed.'))


def register_vote(request):
    secret = PopOrNotSecret()

    if not request.is_ajax():
        return HttpResponseBadRequest()

    if request.method == "POST":
        vote = request.POST.get('vote')
        vote_token = request.POST.get('token')

        # Check make sure we have both the vote and token.
        if vote != 'a' and vote != 'b':
            return HttpResponseBadRequest(json_error_response('No candidate selected.'))

        if not vote_token:
            return HttpResponseBadRequest(json_error_response('Token not found.'))

        # OK, now we have to token, it's time to get the candidates
        candidate_a, candidate_b = secret.get_candidates(vote_token, validate_token=True)

        if candidate_a and candidate_b:
            if vote == 'a':
                winner = candidate_a
            else:
                winner = candidate_b

            # Save the vote
            PopOrNot.objects.create(candidate_a=candidate_a, candidate_b=candidate_b, winner=winner)

            # Get the result
            result, created = PopResult.objects.get_or_create(candidate_a=candidate_a, candidate_b=candidate_b)

            # Invalidate the token so that it can't be reused.
            secret.invalidate_token()
        else:
            return HttpResponseBadRequest(json_error_response('Bad candidates.'))

        popularity_a, score_a = get_popularity_for_candidate(candidate_a)
        popularity_b, score_b = get_popularity_for_candidate(candidate_b)

        # Update the score
        update_score_for_candidate(candidate_a, score_a)
        update_score_for_candidate(candidate_b, score_b)

        response_data = {
            'popularity_a': popularity_a,
            'popularity_b': popularity_b,
            'result_title': result.get_title(),
            'result_tags': result.get_tags(),
            'result_url': result.get_full_url(),
            'result_short_url': result.get_short_url()
        }

        return HttpResponse(json_success_response(response_data), mimetype='application/json')

    else:
        return HttpResponseNotAllowed(json_error_response('Only POST allowed.'))