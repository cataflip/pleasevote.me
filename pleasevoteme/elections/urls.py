from django.conf import settings
from django.conf.urls import *

from .views import register_vote, skip_or_next, VoteResultView, PopOrNotVoteView, CampaignListView, \
    CampaignSingleView, CampaignCoalitionView

urlpatterns = patterns('',
    url(r'^vote/$', PopOrNotVoteView.as_view(), name='vote'),
    url(r'^vote/reg/$', register_vote, name="register_vote"),
    url(r'^vote/skip/$', skip_or_next, name="skip_or_next"),
    url(r'^v/(?P<slug>[-\w]+)/$', VoteResultView.as_view(), name="vote_result"),
    url(r'^campaign/(?:(?P<page>\d+)/)?$', CampaignListView.as_view(), name='campaign'),
    url(r'^campaign/(?P<coalition>[-\w]+)/$', CampaignCoalitionView.as_view(), name='campaign_coalition'),
    url(r'^campaign/(?P<coalition>[-\w]+)/(?P<slug>[-\w]+)/$', CampaignSingleView.as_view(), name='campaign_single'),
)

