import os
import time
import urlparse
import shortuuid
from autoslug import AutoSlugField
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFit
from django.db import models
from django.db.models import permalink, Count
from django.utils import timezone
from django.utils.encoding import smart_unicode
from unidecode import unidecode


from pleasevoteme.parties.models import Candidate, Coalition, Party
from pleasevoteme.helpers.utils import create_short_url, get_site_url


def result_slug(instance):
    return "%s vs %s" % (instance.candidate_a, instance.candidate_b)


def unicoded_caption(instance):
    return unidecode(smart_unicode(instance.title))


def campaign_img_upload_path(instance, filename):
    # Get the epoch time to be appended to the end of the file
    # Cache busting!
    epoch = int(time.mktime(time.gmtime()))

    # save file name as the username
    ext = os.path.splitext(filename)[1]
    filename = '%s_%s_%d%s' % (instance.coalition.abbreviation.lower(), instance.slug.replace("-", "_"), epoch, ext)

    return "campaign/%s" % filename


class OnlineCampaign(models.Model):
    title = models.CharField(max_length=100)
    slug = AutoSlugField(populate_from=lambda instance: unicoded_caption(instance), max_length=60, db_index=True)
    coalition = models.ForeignKey(Coalition, blank=True, null=True, related_name='campaigns')
    party = models.ForeignKey(Party, blank=True, null=True, related_name='party_campaigns')
    description = models.CharField(max_length=2000,  blank=True, null=True)
    content = models.TextField()
    created = models.DateTimeField(default=timezone.now)
    img = models.ImageField(upload_to=campaign_img_upload_path, blank=True, null=True)
    img_thumb = ImageSpecField([ResizeToFit(width=350)], image_field='img')
    source = models.URLField(blank=True, null=True, max_length=1000)

    def __unicode__(self):
        return u'%s by %s' % (self.title, self.coalition)

    @permalink
    def get_absolute_url(self):
        return ('campaign_single', None, {
            'coalition': self.coalition.abbreviation.lower(),
            'slug': self.slug
        })

    def get_full_url(self):
        return urlparse.urljoin(get_site_url(), self.get_absolute_url())


class PopOrNot(models.Model):
    candidate_a = models.ForeignKey(Candidate, related_name='as_a_matches')
    candidate_b = models.ForeignKey(Candidate, related_name='as_b_matches')
    winner = models.ForeignKey(Candidate, blank=True, null=True, related_name='won_matches')
    timestamp = models.DateTimeField(default=timezone.now)

    def save(self, *args, **kwargs):
        if not self.id:
            self.code = shortuuid.uuid()

        super(PopOrNot, self).save(*args, **kwargs)


class PopResult(models.Model):
    candidate_a = models.ForeignKey(Candidate, related_name='as_a_results')
    candidate_b = models.ForeignKey(Candidate, related_name='as_b_results')
    result_slug = AutoSlugField(populate_from=lambda instance: result_slug(instance),
                                db_index=True)
    short_url = models.URLField(blank=True, null=True)

    def __unicode__(self):
        return u'%s or %s' % (self.candidate_a.name, self.candidate_b.name)

    @permalink
    def get_absolute_url(self):
        return ('vote_result', None, {
            'slug': self.result_slug
        })

    def get_full_url(self):
        return urlparse.urljoin(get_site_url(), self.get_absolute_url())

    def get_short_url(self):
        if self.short_url:
            return self.short_url
        else:
            # For some reason, the short url couldn't be saved on post creation
            # Bitly API error maybe, try again
            try:
                short_url = create_short_url(self.get_full_url())
                self.short_url = short_url
                self.save()

                return short_url
            except Exception, e:
                # OK fall back to normal url
                return self.get_full_url()

    def get_title(self):
        return u'GE13: %s vs %s' % (self.candidate_a, self.candidate_b)

    def get_tags(self):
        return u'GE13,PRU13,%s,%s' % (self.candidate_a.party, self.candidate_b.party)


class PopScore(models.Model):
    candidate = models.OneToOneField(Candidate, related_name='score')
    score = models.FloatField(default=0)

    def ranking(self):
        aggregate = PopScore.objects.filter(score__gt=self.score).aggregate(ranking=Count('score'))

        return aggregate['ranking'] + 1

Candidate.score = property(lambda c: PopScore.objects.get_or_create(candidate=c)[0])