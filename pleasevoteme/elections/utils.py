# http://www.codekoala.com/blog/2009/aes-encryption-python-using-pycrypto/

from Crypto.Cipher import AES
import base64
import redis
import uuid
import hashlib
from annoying.functions import get_object_or_None
from django.conf import settings
from django.db.models import Q
from.models import Candidate
from pleasevoteme.elections.models import PopOrNot, PopScore

# the block size for the cipher object; must be 16, 24, or 32 for AES
AES_BLOCK_SIZE = 32

# the character used for padding--with a block cipher such as AES, the value
# you encrypt must be a multiple of BLOCK_SIZE in length.  This character is
# used to ensure that your value is always a multiple of BLOCK_SIZE
PADDING = '%'

SECRET_KEY = getattr(settings, 'POPORNOT_SECRET_KEY', None)
REDIS_HOST = getattr(settings, 'POPORNOT_REDIS_HOST', '127.0.0.1')
REDIS_PORT = getattr(settings, 'POPORNOT_REDIS_PORT', 6379)
REDIS_DB = getattr(settings, 'POPORNOT_REDIS_DBT', 0)
REDIS_TTL = getattr(settings, 'POPORNOT_REDIS_TTL', 20)


class PopOrNotSecret(object):
    """
    Use AES to encrypt and decrypt the candidate IDs and the timestamp (in epoch)
    """
    def __init__(self):
        """
        Construct a Secret object.
        """
        self.secret_key = settings.POPORNOT_SECRET_KEY
        self.cipher = AES.new(self.secret_key)
        self.token = None
        self.redis = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

        assert self.secret_key is not None, "Secret key must be provided!"

    def _padding(self, message):
        return message + (AES_BLOCK_SIZE - len(message) % AES_BLOCK_SIZE) * PADDING

    def _decrypt(self, encrypted_token):
        """
        Decrypt.

        @param encrypted_token: The encrypted token to decrypt.
        @return: Plain text token
        """

        return self.cipher.decrypt(base64.b64decode(encrypted_token)).rstrip(PADDING)

    def _encrypt_token(self):
        """
        Encrypt.

        @return: Encrypted token
        """
        assert self.token is not None, 'Token is none!'

        return base64.b64encode(self.cipher.encrypt(self._padding(self.token)))

    def generate_token(self, candidate_a, candidate_b):
        """
        Generate a token in format unique_key+candidate_a_id|candidate_b_id
        @param candidate_a: Candidate object.
        @param candidate_b: Candidate object.
        @return: Encrypted token
        """
        unique_key = hashlib.sha1(str(uuid.uuid4())).hexdigest()

        # Construct the plain text token
        token = '%s+%s' % (unique_key, '%s|%s' % (candidate_a.id, candidate_b.id))
        self.token = token

        # Now set the key into Redis
        self._set_candidate_ids()

        # Now encrypt the token. This is what the client gets
        encrypted_token = self._encrypt_token()

        return encrypted_token

    def _split_token(self):
        """
        Slpit the token into unique_key:candidate_ids
        @return: Tuple of unique_key, candidate_ids
        """
        assert self.token is not None, 'Token is none!'

        try:
            unique_key, candidate_ids = self.token.split('+')
        except ValueError:
            unique_key = None
            candidate_ids = None

        return unique_key, candidate_ids

    def validate_token(self, encrypted_token):
        """
        Validate the encrypted token.
        @param encrypted_token: The encrypted token
        @return: string of candidate_a_id|candidate_b_id or None
        """
        token = self._decrypt(encrypted_token)
        self.token = token

        # Now check and return the candidate_ids
        return self._get_candidate_ids()

    def invalidate_token(self):
        """
        Invalidate the current token to prevent reuse.
        """
        self._delete_candidate_ids()

    def _set_candidate_ids(self):
        """
        Key: unique_key
        Value: candidate_a_id|candidate_b_id
        Save the key:value pair temporarily to Redis that auto expires after REDIS_TTL.
        """
        unique_key, candidate_ids = self._split_token()

        self.redis.set(unique_key, candidate_ids)
        self.redis.expire(unique_key, REDIS_TTL)

    def _delete_candidate_ids(self):
        """
        Key: unique_key
        Value: candidate_a_id|candidate_b_id
        Delete the key:value pair from Redis
        """
        unique_key, candidate_ids = self._split_token()

        self.redis.delete(unique_key)

    def _get_candidate_ids(self):
        """
        Get the key:value from Redis, None if already expired.
        @return: candidate_a_id|candidate_b_id or None
        """
        unique_key, candidate_ids = self._split_token()

        if unique_key and self.redis.get(unique_key) == candidate_ids:
            return candidate_ids
        else:
            return None

    def get_candidates(self, vote_token, validate_token=True):
        """
        Decrypt the vote_token and return the candidate's objects
        @param vote_token: The encrypted token
        @param validate_token: Token requires validation
        @return: a tuple of candidate objects
        """

        if validate_token:
            candidate_ids = self.validate_token(vote_token)
        else:
            # We do not query redis to check its validity
            # as we only concern about the candidate ids
            token = self._decrypt(vote_token)
            self.token = token

            unique_key, candidate_ids = self._split_token()

        if candidate_ids:
            try:
                a_id, b_id = candidate_ids.split('|')

                candidate_a_id = int(a_id)
                candidate_b_id = int(b_id)

            except ValueError:
                return None, None

            candidate_a = get_object_or_None(Candidate, id=candidate_a_id)
            candidate_b = get_object_or_None(Candidate, id=candidate_b_id)

            return candidate_a, candidate_b

        else:
            return None, None


def get_popularity_for_candidate(candidate):

    qs = PopOrNot.objects.filter(Q(candidate_a=candidate) | Q(candidate_b=candidate))
    matches = qs.count()
    wins = qs.filter(winner=candidate).count()
    score = float(wins) / float(matches)
    print type(score)
    popularity = int(round(100 * score))

    return popularity, score


def update_score_for_candidate(candidate, score):
    pop_score = candidate.score
    pop_score.score = score
    pop_score.save()