import re
import json
from time import time

from django.views.generic.base import TemplateView
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponseBadRequest, HttpResponseNotAllowed, HttpResponse, HttpResponseNotFound, \
    HttpResponseServerError
from django.core.exceptions import ImproperlyConfigured
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.http import http_date


# Ajax helper views
def json_error_response(error_message):
    return json.dumps(dict(success=False, error_message=error_message), cls=DjangoJSONEncoder)


def json_success_response(data):
    json_data = dict(success=True)
    json_data.update(data)

    return json.dumps(json_data, cls=DjangoJSONEncoder)


class AjaxableResponseMixin(object):
    """
    Mixin to add general AJAX support to a view.
    """
    def render_to_json_response(self, context, **response_kwargs):
        context['success'] = True
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'

        return HttpResponse(data, **response_kwargs)

    def render_json_error_response(self, error_type, **response_kwargs):

        if error_type == 404:
            error_message = 'Not found.'
            response = HttpResponseNotFound
        elif error_type == 400:
            error_message = 'Bad request.'
            response = HttpResponseBadRequest
        elif error_type == 405:
            error_message = 'Not allowed.'
            response = HttpResponseNotAllowed
        else:
            error_message = 'Unknown error.'
            response = HttpResponse

        data = json.dumps(dict(success=False, error_message=error_message))
        response_kwargs['content_type'] = 'application/json'

        return response(data, **response_kwargs)

    def render_json_form_error_response(self, form_errors, **response_kwargs):
        data = json.dumps(dict(success=False, form_errors=form_errors))
        response_kwargs['content_type'] = 'application/json'

        return HttpResponse(data, **response_kwargs)


class TextPlainView(TemplateView):
    """
    Render a plain text view from a template
    i.e. rendering a robots.txt or humans.txt
    """
    def render_to_response(self, context, **kwargs):
        return super(TextPlainView, self).render_to_response(
            context, content_type='text/plain', **kwargs)


class TemplateCachedView(TemplateView):
    """
    Template view but cached
    """
    def get(self, request, *args, **kwargs):
        response = super(TemplateCachedView, self).get(request, *args, **kwargs)

        max_age = 31536000

        response['Cache-Control'] = 'max-age=%s, public' % max_age
        response['Expires'] = http_date(time() + max_age)

        return response


def handler500(request):
    return HttpResponseServerError(render_to_string('500.html',
                                                    context_instance=RequestContext(request)))


def handler404(request):
    return HttpResponseNotFound(render_to_string('404.html',
                                                 context_instance=RequestContext(request)))