import gc

from django.utils.functional import lazy
from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator
from django.db import connection
from django.db.models import AutoField
from django.conf import settings

from pleasevoteme.helpers import bitly

# http://djangosnippets.org/snippets/2445/
# Workaround for using reverse with success_url in class based generic views
# because direct usage of it throws an exception.
reverse_lazy = lambda name=None, *args: lazy(reverse, str)(name, args=args)


def view_decorator(orig_dec):
    """
        Convert the provided decorator to one that can be applied to a view
        class (ie. automatically decorates dispatch) with writing over
        and over again for every class-based view

        # usage
        from foo.bar import view_decorator
        from django.contrib.auth.decorators import login_required

        @view_decorator(login_required)
        class MyView(base.TemplateView):
            pass
    """

    # We're going to be applying a regular decorator to a method, so the first
    # step is to convert it to a method decorator.
    method_dec = method_decorator(orig_dec)

    # This is the decorator we're actually going to return. Since we're
    # returning a class decorator, it takes a single argument, the class
    # that it's decorating. It therefore returns this as well.
    def dec(cls):
        # We're about to change what cls.dispatch refers to, so we need to
        # keep a reference to the original dispatch around so that we can
        # call it later.
        orig_dispatch = cls.dispatch
        def _dispatch(self, *args, **kwargs):
            # Right - decorate the original dispatch method using our new,
            # method-ised version of the decorator we were passed in to start
            # with
            decorated = method_dec(orig_dispatch)

            # Finally, we can call the decorated dispatch() method.
            return decorated(self, *args, **kwargs)

        # Replace the original dispatch with our new dispatch function. We
        # kept a reference to the old dispatch method earlier, in a closure.
        cls.dispatch = _dispatch
        return cls
    return dec


# Check if table exists
def db_table_exists(table, cursor=None):
    try:
        if not cursor:
            cursor = connection.cursor()
        if not cursor:
            return False
        table_names = connection.introspection.get_table_list(cursor)
    except:
        return False
    else:
        return table in table_names


def copy_model_instance(obj):
    """
    Create a copy of a model instance.

    M2M relationships are currently not handled, i.e. they are not
    copied.

    See also Django #4027.
    """
    initial = dict([(f.name, getattr(obj, f.name))
                    for f in obj._meta.fields
                    if not isinstance(f, AutoField) and not f in obj._meta.parents.values()])
    return obj.__class__(**initial)


def get_site_url():
    """
    Returns fully qualified URL (no trailing slash) for the current site.
    """
    from django.contrib.sites.models import Site
    current_site = Site.objects.get_current()
    protocol = getattr(settings, 'MY_SITE_PROTOCOL', 'http')
    port = getattr(settings, 'MY_SITE_PORT', '')
    url = '%s://%s' % (protocol, current_site.domain)
    if port:
        url += ':%s' % port
    return url


def smart_truncate(content, length=100, suffix='...'):
    if len(content) <= length:
        return content
    else:
        return ' '.join(content[:length + 1].split(' ')[0:-1]) + suffix


def create_short_url(long_url):
    """
    Create a short url using Bitly's API
    """
    if settings.DEBUG:
        raise Exception("Can't shorten URL when settings.DEBUG = True")
    api = bitly.Api(login=settings.BITLY_LOGIN, apikey=settings.BITLY_API_KEY)
    short_url = api.shorten(long_url)

    return short_url


def queryset_iterator(queryset, chunksize=1000):
    """
    Iterate over a Django Queryset ordered by the primary key

    This method loads a maximum of chunksize (default: 1000) rows in it's
    memory at the same time while django normally would load all rows in it's
    memory. Using the iterator() method only causes it to not preload all the
    classes.

    Note that the implementation of the iterator does not support ordered query sets.
    """
    pk = 0
    last_pk = queryset.order_by('-pk')[0].pk
    queryset = queryset.order_by('pk')
    while pk < last_pk:
        print "Processing entry with pk... %s" % pk
        for row in queryset.filter(pk__gt=pk)[:chunksize]:
            pk = row.pk
            yield row
        gc.collect()
