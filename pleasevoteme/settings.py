# Django settings for pleasevoteme project.

import os.path

PROJECT_PATH = '/home/undi/www/pleasevote.me/pleasevoteme/'
PROJECT_WORKING_DIR = '/home/undi/www/pleasevote.me/'

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('PVM Team', 'hello@pleasevote.me'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'pvmdb',
        'USER': 'ecadmin',
        'PASSWORD': 'po8d3OPjGWm1wl6lFgOj',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['.pleasevote.me', 'localhost', '127.0.0.1']

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'UTC'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_WORKING_DIR, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = 'http://media.pleasevote.me/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = os.path.join(PROJECT_WORKING_DIR, 'static')

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = 'http://static.pleasevote.me/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_PATH, 'static'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

# File storage

DEFAULT_FILE_STORAGE = 'pleasevoteme.storage.OverwriteFileStorage'

# Make this unique, and don't share it with anybody.
SECRET_KEY = '@=ss6^%ne#tpagygqprt7@*#%kp-j7(l!7yzyp68@by1rz9_33'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'pleasevoteme.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'pleasevoteme.wsgi.application'

INTERNAL_IPS = (
    '127.0.0.1',
    '10.0.2.2',
)

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_PATH, 'templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.sitemaps',
    'django_extensions',
    'south',
    'imagekit',
    'compressor',
    'ckeditor',
    'paginationplus',
    'pleasevoteme.states',
    'pleasevoteme.seats',
    'pleasevoteme.parties',
    'pleasevoteme.elections',
    'pleasevoteme.csvimport',
    'raven.contrib.django.raven_compat',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'WARNING',
        'handlers': ['sentry'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
            },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
            },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
    },
}

RAVEN_CONFIG = {
    'dsn': 'https://a7118986576544a9a75950103f4109ea:388029a83f414c7283ae5131bbf46e12@pleasevoteme-sentry.herokuapp.com/2',
}

# Caching

CACHES = {
    "default": {
        "BACKEND": "redis_cache.cache.RedisCache",
        "LOCATION": "127.0.0.1:6379:1",
        "OPTIONS": {
            "CLIENT_CLASS": "redis_cache.client.DefaultClient",
        }
    }
}

# Session
SESSION_COOKIE_AGE = 604800  # 1 week, default is 2 weeks
SESSION_ENGINE = 'redis_sessions.session'
SESSION_REDIS_HOST = '127.0.0.1'
SESSION_REDIS_PORT = 6379
SESSION_REDIS_DB = 2
SESSION_REDIS_PREFIX = 'session'

# Temp PopOrNot key storage

POPORNOT_REDIS_HOST = '127.0.0.1'
POPORNOT_REDIS_PORT = 6379
POPORNOT_REDIS_DB = 3
POPORNOT_REDIS_TTL = 30

# DJANGO COMPRESSOR #

COMPRESS_URL = STATIC_URL
COMPRESS_ROOT = STATIC_ROOT
COMPRESS_OUTPUT_DIR = ''
COMPRESS_JS_FILTERS = ['compressor.filters.closure.ClosureCompilerFilter']
COMPRESS_CSS_FILTERS = ['compressor.filters.yui.YUICSSFilter']
COMPRESS_CLOSURE_COMPILER_BINARY = 'java -jar %s' % os.path.join(PROJECT_PATH, 'compiler.jar')
COMPRESS_YUI_BINARY = 'java -jar %s' % os.path.join(PROJECT_PATH, 'yuicompressor.jar')
COMPRESS_OFFLINE = True

# CKEDITOR #

CKEDITOR_UPLOAD_PATH = os.path.join(MEDIA_ROOT, 'upload')
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Full',
    },
}

# API keys

BITLY_LOGIN = 'pleasevoteme'
BITLY_API_KEY = 'R_78c97a3fcae9fcedc7d3eeda3b4538ce'
POPORNOT_SECRET_KEY = 'ro0n-eI!*vA}SnvETwesI|u[6.0y~XNl'


# Autoslug
AUTOSLUG_SLUGIFY_FUNCTION = 'django.template.defaultfilters.slugify'


### Import dev settings if exists ###

try:
    from settings_dev import *
except ImportError:
    pass