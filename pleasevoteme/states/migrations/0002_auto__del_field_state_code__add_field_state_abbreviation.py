# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'State.code'
        db.delete_column(u'states_state', 'code')

        # Adding field 'State.abbreviation'
        db.add_column(u'states_state', 'abbreviation',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=3),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'State.code'
        raise RuntimeError("Cannot reverse this migration. 'State.code' and its values cannot be restored.")
        # Deleting field 'State.abbreviation'
        db.delete_column(u'states_state', 'abbreviation')


    models = {
        u'states.state': {
            'Meta': {'object_name': 'State'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'flag': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['states']