from django.db import models
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFit


class State(models.Model):
    abbreviation = models.CharField(max_length=3)
    name = models.CharField(max_length=20)
    flag = models.ImageField(upload_to='state')
    flag_thumb = ImageSpecField([ResizeToFit(width=100)], image_field='flag', options={'quality': 90})

    def __unicode__(self):
        return u'%s' % self.name