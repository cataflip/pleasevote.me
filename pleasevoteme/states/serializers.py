from rest_framework import serializers


class StateSerializer(serializers.Serializer):
    abbreviation = serializers.CharField(max_length=3)
    name = serializers.CharField(max_length=20)
    flag = serializers.Field(source='flag.url')
    flag_thumb = serializers.Field(source='flag_thumb.url')
